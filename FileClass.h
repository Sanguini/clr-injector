#pragma once
#include <iostream>
#include <fstream>


class fileVarClass {
public:
	bool shallLoad;
	bool autoInject;
	int waitTime;
	std::string dllName;
	std::string exeName;
};

class FileClass {
private:
	bool stringToBool(std::string);

	bool wasAutoLoaded = false;

	std::ofstream saveHandle;
	std::ifstream loadHandle;
public:
	FileClass(std::string fileName);
	fileVarClass* loadConfig();			//ONLY CALL ONCE!
	void saveConfig(fileVarClass*);
	void shutdown();
};