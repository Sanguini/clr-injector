#include "FileClass.h"
#include <string>


bool FileClass::stringToBool(std::string String) {
	if (String[0] == '0')
		return true;
	return false;
}

FileClass::FileClass(std::string fileName)
{
	this->loadHandle.open(fileName);
	
	if (this->loadHandle.good() && !this->loadHandle.eof())
	{
		std::string temp;
		std::getline(this->loadHandle, temp);
		this->saveHandle.open(fileName, std::ios::app);	//Dont delete the file (yet)
		if (this->stringToBool(temp)) {
			this->wasAutoLoaded = true;
		}
	}

}

fileVarClass* FileClass::loadConfig()
{
	fileVarClass* fileVars = new fileVarClass();
	fileVars->shallLoad = this->wasAutoLoaded;
	if (!this->loadHandle.eof() && this->loadHandle.good())
	{
		std::string temp;
		std::getline(this->loadHandle, temp);
		fileVars->autoInject = this->stringToBool(temp);
		std::getline(this->loadHandle, temp);
		fileVars->waitTime = atoi(temp.c_str());
		std::getline(this->loadHandle, temp);
		fileVars->dllName = temp;
		std::getline(this->loadHandle, temp);
		fileVars->exeName = temp;
	}
	return fileVars;
}

void FileClass::saveConfig(fileVarClass* fVC)
{
	this->saveHandle.clear();
	this->saveHandle << fVC->shallLoad << std::endl << fVC->autoInject << std::endl << fVC->waitTime << std::endl
		<< fVC->dllName << std::endl << fVC->exeName << std::endl;
	this->saveHandle.flush();
}

void FileClass::shutdown()
{
	this->saveHandle.close();
	this->loadHandle.close();
}