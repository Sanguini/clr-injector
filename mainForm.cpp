#include "mainForm.h"

using namespace Inject0r;

int bootUI()
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	mainForm^ mainUi = gcnew mainForm();
	Application::Run(mainUi);

	return true;
}

[STAThread]
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	PSTR lpCmdLine, INT nCmdShow) {
	return bootUI();
}