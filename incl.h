#include <Windows.h>
#include <iostream>
#include <TlHelp32.h>
#include <fstream>
#include <time.h>
#include <msclr\marshal_cppstd.h>
#include "processStruct.h"
#include "OptionClass.h"
#include "OptionForm.h"
#include "processFinder.h"
#include "FileClass.h"

#pragma comment(lib,"Advapi32.lib")
#pragma comment(lib,"User32.lib")

class mainClass {
public:
	void EnableDebugPriv();
	bool Inject();
	bool isProcessAndFillIn();

	OptionClass* options = new OptionClass();
	processStruct* currentProcess = new processStruct();
	std::string fileName;
	bool wasFileValidated;

	bool wasAutoInjectTriggered = false;
	int autoInjectTmr;

	std::string errorMsg;
};

extern mainClass* base;

void mainClass::EnableDebugPriv()
{
	HANDLE hToken;
	LUID luid;
	TOKEN_PRIVILEGES tkp;

	OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken);

	LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luid);

	tkp.PrivilegeCount = 1;
	tkp.Privileges[0].Luid = luid;
	tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	AdjustTokenPrivileges(hToken, false, &tkp, sizeof(tkp), NULL, NULL);

	CloseHandle(hToken);
}

bool mainClass::Inject()
{
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, base->currentProcess->processId);

	LPVOID addr = (LPVOID)GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");

	if (addr == NULL) {
		base->errorMsg = "LoadLibraryA function was not found inside kernel32.dll library.";
		return false;
	}

	LPVOID arg = (LPVOID)VirtualAllocEx(hProcess, NULL, base->fileName.size(), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if (arg == NULL) {
		base->errorMsg = "Memory could not be allocated inside the chosen process.";
		return false;
	}

	int n = WriteProcessMemory(hProcess, arg, base->fileName.c_str(), base->fileName.size(), NULL);
	if (n == 0) {
		base->errorMsg = "No bytes where written to the process's address space.";
		return false;
	}

	HANDLE threadID = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)addr, arg, NULL, NULL);
	if (threadID == NULL) {
		base->errorMsg = "Remote thread could not be created.";
		return false;
	}
	else {
		return true;
	}

	CloseHandle(hProcess);
}

bool mainClass::isProcessAndFillIn()
{
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	if (Process32First(snapshot, &entry) == TRUE)
	{
		while (Process32Next(snapshot, &entry) == TRUE)
		{
			if (strcmp(entry.szExeFile, base->currentProcess->processName.c_str()) == 0)
			{
				base->currentProcess->processId = entry.th32ProcessID;
				base->currentProcess->wasProcessValidated = true;
				return true;
			}
		}
	}
	base->currentProcess->wasProcessValidated = false;
	return false;
}
