#pragma once
#include <Windows.h>
#include <TlHelp32.h>
#include <Psapi.h>
#include <iostream>
#include <array>
#include <msclr\marshal_cppstd.h>

namespace Inject0r {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r processFinder
	/// </summary>
	public ref class processFinder : public System::Windows::Forms::Form
	{
	public:
		processFinder(void)
		{
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~processFinder()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::CheckedListBox^  checkedListBox1;
	private: System::Windows::Forms::Button^  selectBtn;

	private: System::Windows::Forms::Timer^  processFindTmr;
	private: System::Windows::Forms::Timer^  oneCheckedTimer;
	public: System::Windows::Forms::Label^  procNameLbl;
	private:

	private: System::ComponentModel::IContainer^  components;
	protected:

	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->checkedListBox1 = (gcnew System::Windows::Forms::CheckedListBox());
			this->selectBtn = (gcnew System::Windows::Forms::Button());
			this->processFindTmr = (gcnew System::Windows::Forms::Timer(this->components));
			this->oneCheckedTimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->procNameLbl = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// checkedListBox1
			// 
			this->checkedListBox1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->checkedListBox1->FormattingEnabled = true;
			this->checkedListBox1->Location = System::Drawing::Point(0, 0);
			this->checkedListBox1->Name = L"checkedListBox1";
			this->checkedListBox1->Size = System::Drawing::Size(179, 182);
			this->checkedListBox1->TabIndex = 0;
			// 
			// selectBtn
			// 
			this->selectBtn->Location = System::Drawing::Point(0, 183);
			this->selectBtn->Name = L"selectBtn";
			this->selectBtn->Size = System::Drawing::Size(179, 34);
			this->selectBtn->TabIndex = 1;
			this->selectBtn->Text = L"Select";
			this->selectBtn->UseVisualStyleBackColor = true;
			this->selectBtn->Click += gcnew System::EventHandler(this, &processFinder::button1_Click);
			// 
			// processFindTmr
			// 
			this->processFindTmr->Enabled = true;
			this->processFindTmr->Interval = 250;
			this->processFindTmr->Tick += gcnew System::EventHandler(this, &processFinder::processFindTmr_Tick);
			// 
			// oneCheckedTimer
			// 
			this->oneCheckedTimer->Enabled = true;
			this->oneCheckedTimer->Tick += gcnew System::EventHandler(this, &processFinder::oneCheckedTimer_Tick);
			// 
			// procNameLbl
			// 
			this->procNameLbl->AutoSize = true;
			this->procNameLbl->Location = System::Drawing::Point(117, 9);
			this->procNameLbl->Name = L"procNameLbl";
			this->procNameLbl->Size = System::Drawing::Size(0, 13);
			this->procNameLbl->TabIndex = 2;
			this->procNameLbl->Visible = false;
			// 
			// processFinder
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(180, 218);
			this->Controls->Add(this->procNameLbl);
			this->Controls->Add(this->selectBtn);
			this->Controls->Add(this->checkedListBox1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"processFinder";
			this->ShowIcon = false;
			this->ShowInTaskbar = false;
			this->Text = L"Select Process";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	public: System::String^ procName;
	private: System::Void processFindTmr_Tick(System::Object^  sender, System::EventArgs^  e) {
		PROCESSENTRY32 entry;
		entry.dwSize = sizeof(PROCESSENTRY32);
		std::array<std::string,1000> procNames;
		int i = 0;
		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
		if (Process32First(snapshot, &entry) == TRUE)
		{
			while (Process32Next(snapshot, &entry) == TRUE)
			{
				procNames.at(i) = entry.szExeFile;
				i++;
			}
		}

		for (int x = 0; x < i; x++)
		{
			this->checkedListBox1->Items->Insert(x, gcnew String(procNames.at(x).c_str()));
		}
		this->processFindTmr->Enabled = false;
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		IEnumerator^ myEnum2 = checkedListBox1->CheckedItems->GetEnumerator();
		while ( myEnum2->MoveNext() )
		{
			Object^ itemChecked = safe_cast<Object^>(myEnum2->Current);
			this->procNameLbl->Text = itemChecked->ToString();
			break;
		}
		this->Close();
	}
		 int lastChecked = 0;
	private: System::Void oneCheckedTimer_Tick(System::Object^  sender, System::EventArgs^  e) {
		if (checkedListBox1->CheckedIndices->Count > 1)
		{
			IEnumerator^ myEnum1 = checkedListBox1->CheckedIndices->GetEnumerator();	

			while ( myEnum1->MoveNext() )
		   {
			  Int32 indexChecked =  *safe_cast<Int32^>(myEnum1->Current);
			  if (indexChecked != lastChecked)
			  {
				  checkedListBox1->SetItemChecked(lastChecked, false);
				  lastChecked = indexChecked;
				  break;
			  }
			}
		}
	}
};
}
