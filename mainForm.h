#pragma once
#include "incl.h"

mainClass* base = new mainClass();
FileClass* fileClass;

namespace Inject0r {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r mainForm
	/// </summary>
	public ref class mainForm : public System::Windows::Forms::Form
	{
	public:
		mainForm(void)
		{
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~mainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  procNameTextBox;
	private: System::Windows::Forms::TextBox^  dllLocTextBox;
	protected:

	private: System::Windows::Forms::Label^  procNameLbl;
	private: System::Windows::Forms::Label^  dllNameLbl;
	private: System::Windows::Forms::Label^  procFoundLbl;
	private: System::Windows::Forms::Button^  injectBtn;
	private: System::Windows::Forms::Button^  settingBtn;
	private: System::Windows::Forms::Button^  procFindBtn;

	private: System::Windows::Forms::Button^  openFileBtn;
	private: System::Windows::Forms::Timer^  updateTimer;
	private: System::Windows::Forms::Label^  dllLocFoundLbl;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog;
	private: System::Windows::Forms::Label^  autoInjectTmrLbl;




	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->procNameTextBox = (gcnew System::Windows::Forms::TextBox());
			this->dllLocTextBox = (gcnew System::Windows::Forms::TextBox());
			this->procNameLbl = (gcnew System::Windows::Forms::Label());
			this->dllNameLbl = (gcnew System::Windows::Forms::Label());
			this->procFoundLbl = (gcnew System::Windows::Forms::Label());
			this->injectBtn = (gcnew System::Windows::Forms::Button());
			this->settingBtn = (gcnew System::Windows::Forms::Button());
			this->procFindBtn = (gcnew System::Windows::Forms::Button());
			this->openFileBtn = (gcnew System::Windows::Forms::Button());
			this->updateTimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->dllLocFoundLbl = (gcnew System::Windows::Forms::Label());
			this->openFileDialog = (gcnew System::Windows::Forms::OpenFileDialog());
			this->autoInjectTmrLbl = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// procNameTextBox
			// 
			this->procNameTextBox->Location = System::Drawing::Point(46, 34);
			this->procNameTextBox->Name = L"procNameTextBox";
			this->procNameTextBox->Size = System::Drawing::Size(226, 20);
			this->procNameTextBox->TabIndex = 0;
			// 
			// dllLocTextBox
			// 
			this->dllLocTextBox->Location = System::Drawing::Point(46, 112);
			this->dllLocTextBox->Name = L"dllLocTextBox";
			this->dllLocTextBox->Size = System::Drawing::Size(226, 20);
			this->dllLocTextBox->TabIndex = 1;
			// 
			// procNameLbl
			// 
			this->procNameLbl->AutoSize = true;
			this->procNameLbl->Location = System::Drawing::Point(43, 18);
			this->procNameLbl->Name = L"procNameLbl";
			this->procNameLbl->Size = System::Drawing::Size(79, 13);
			this->procNameLbl->TabIndex = 2;
			this->procNameLbl->Text = L"Process Name:";
			// 
			// dllNameLbl
			// 
			this->dllNameLbl->AutoSize = true;
			this->dllNameLbl->Location = System::Drawing::Point(43, 96);
			this->dllNameLbl->Name = L"dllNameLbl";
			this->dllNameLbl->Size = System::Drawing::Size(66, 13);
			this->dllNameLbl->TabIndex = 3;
			this->dllNameLbl->Text = L"Dll Location:";
			// 
			// procFoundLbl
			// 
			this->procFoundLbl->AutoSize = true;
			this->procFoundLbl->ForeColor = System::Drawing::Color::Red;
			this->procFoundLbl->Location = System::Drawing::Point(43, 57);
			this->procFoundLbl->Name = L"procFoundLbl";
			this->procFoundLbl->Size = System::Drawing::Size(99, 13);
			this->procFoundLbl->TabIndex = 4;
			this->procFoundLbl->Text = L"Process not Found!";
			// 
			// injectBtn
			// 
			this->injectBtn->Enabled = false;
			this->injectBtn->Location = System::Drawing::Point(197, 161);
			this->injectBtn->Name = L"injectBtn";
			this->injectBtn->Size = System::Drawing::Size(75, 23);
			this->injectBtn->TabIndex = 6;
			this->injectBtn->Text = L"Inject";
			this->injectBtn->UseVisualStyleBackColor = true;
			this->injectBtn->Click += gcnew System::EventHandler(this, &mainForm::injectBtn_Click);
			// 
			// settingBtn
			// 
			this->settingBtn->Location = System::Drawing::Point(21, 161);
			this->settingBtn->Name = L"settingBtn";
			this->settingBtn->Size = System::Drawing::Size(75, 23);
			this->settingBtn->TabIndex = 7;
			this->settingBtn->Text = L"Settings";
			this->settingBtn->UseVisualStyleBackColor = true;
			this->settingBtn->Click += gcnew System::EventHandler(this, &mainForm::settingBtn_Click);
			// 
			// procFindBtn
			// 
			this->procFindBtn->Location = System::Drawing::Point(11, 34);
			this->procFindBtn->Name = L"procFindBtn";
			this->procFindBtn->Size = System::Drawing::Size(26, 23);
			this->procFindBtn->TabIndex = 8;
			this->procFindBtn->Text = L"...";
			this->procFindBtn->UseVisualStyleBackColor = true;
			this->procFindBtn->Click += gcnew System::EventHandler(this, &mainForm::procFindBtn_Click);
			// 
			// openFileBtn
			// 
			this->openFileBtn->Location = System::Drawing::Point(11, 110);
			this->openFileBtn->Name = L"openFileBtn";
			this->openFileBtn->Size = System::Drawing::Size(26, 23);
			this->openFileBtn->TabIndex = 9;
			this->openFileBtn->Text = L"...";
			this->openFileBtn->UseVisualStyleBackColor = true;
			this->openFileBtn->Click += gcnew System::EventHandler(this, &mainForm::openFileBtn_Click);
			// 
			// updateTimer
			// 
			this->updateTimer->Enabled = true;
			this->updateTimer->Tick += gcnew System::EventHandler(this, &mainForm::updateTimer_Tick);
			// 
			// dllLocFoundLbl
			// 
			this->dllLocFoundLbl->AutoSize = true;
			this->dllLocFoundLbl->Cursor = System::Windows::Forms::Cursors::Default;
			this->dllLocFoundLbl->ForeColor = System::Drawing::Color::Red;
			this->dllLocFoundLbl->Location = System::Drawing::Point(43, 135);
			this->dllLocFoundLbl->Name = L"dllLocFoundLbl";
			this->dllLocFoundLbl->Size = System::Drawing::Size(73, 13);
			this->dllLocFoundLbl->TabIndex = 10;
			this->dllLocFoundLbl->Text = L"Dll not Found!";
			// 
			// openFileDialog
			// 
			this->openFileDialog->Filter = L"Dll files (*.dll)|*.dll|All files (*.*)|*.*";
			this->openFileDialog->InitialDirectory = L"c:\\";
			this->openFileDialog->RestoreDirectory = true;
			// 
			// autoInjectTmrLbl
			// 
			this->autoInjectTmrLbl->AutoSize = true;
			this->autoInjectTmrLbl->Location = System::Drawing::Point(130, 171);
			this->autoInjectTmrLbl->Name = L"autoInjectTmrLbl";
			this->autoInjectTmrLbl->Size = System::Drawing::Size(29, 13);
			this->autoInjectTmrLbl->TabIndex = 11;
			this->autoInjectTmrLbl->Text = L"0 ms";
			this->autoInjectTmrLbl->Visible = false;
			// 
			// mainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 198);
			this->Controls->Add(this->autoInjectTmrLbl);
			this->Controls->Add(this->dllLocFoundLbl);
			this->Controls->Add(this->openFileBtn);
			this->Controls->Add(this->procFindBtn);
			this->Controls->Add(this->settingBtn);
			this->Controls->Add(this->injectBtn);
			this->Controls->Add(this->procFoundLbl);
			this->Controls->Add(this->dllNameLbl);
			this->Controls->Add(this->procNameLbl);
			this->Controls->Add(this->dllLocTextBox);
			this->Controls->Add(this->procNameTextBox);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"mainForm";
			this->ShowIcon = false;
			this->Text = L"Inject0r";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


	private: System::Void updateTimer_Tick(System::Object^  sender, System::EventArgs^  e) {
		//IF TEXTBOX CHANGED SEARCH FOR PROCESS AND VALIDATE (and for file)
		std::string fileName = msclr::interop::marshal_as<std::string>(dllLocTextBox->Text);
			if (fileName != base->fileName)
			{
				base->fileName = fileName;
			}

			std::string procName = msclr::interop::marshal_as<std::string>(procNameTextBox->Text);

			if (procName != base->currentProcess->processName)
			{
				base->currentProcess->processName = procName;
			}

			std::ifstream inp;
			inp.open(base->fileName);
			if (inp.good())
				base->wasFileValidated = true;
			else
				base->wasFileValidated = false;
			inp.close();
			if (base->wasFileValidated) {
				dllLocFoundLbl->Text = "Dll Found!";
				dllLocFoundLbl->ForeColor = Color::Lime;
			}
			else {
				dllLocFoundLbl->Text = "Dll not Found!";
				dllLocFoundLbl->ForeColor = Color::Red;
			}

			if (base->isProcessAndFillIn())
			{
				procFoundLbl->Text = "Process Found!";
				procFoundLbl->ForeColor = Color::Lime;
			}
			else {
				procFoundLbl->Text = "Process not Found!";
				procFoundLbl->ForeColor = Color::Red;
			}

			if (base->wasFileValidated && base->currentProcess->wasProcessValidated) {
				injectBtn->Enabled = true;
				if (base->wasAutoInjectTriggered && clock() - base->autoInjectTmr > base->options->waitTime)
				{
					base->EnableDebugPriv();
					if (base->Inject())
					{
						exit(0);
						injectBtn->Enabled = false;
					}
					else {
						MessageBoxA(NULL, base->errorMsg.c_str(), "Error!", MB_ICONERROR | MB_OK);
					}
					
				}
				else {
					System::String^ temp = System::Convert::ToString(base->options->waitTime - (clock() - base->autoInjectTmr));
					temp += " ms";
					autoInjectTmrLbl->Text = temp;
				}

				if (base->options->shouldAutoInject && !base->wasAutoInjectTriggered)
				{
					base->autoInjectTmr = clock();
					base->wasAutoInjectTriggered = true;
					autoInjectTmrLbl->Visible = true;
				}
			}
			else {
				injectBtn->Enabled = false;
				base->wasAutoInjectTriggered = false;
				autoInjectTmrLbl->Visible = false;
			}
		}

	private: System::Void procFindBtn_Click(System::Object^  sender, System::EventArgs^  e) {
		processFinder^ procFinder = gcnew processFinder();
		procFinder->ShowDialog();
		procNameTextBox->Text = procFinder->procNameLbl->Text;
	}

	private: System::Void openFileBtn_Click(System::Object^  sender, System::EventArgs^  e) {
		if (openFileDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			dllLocTextBox->Text = openFileDialog->FileName;
		}
	}


	private: System::Void injectBtn_Click(System::Object^  sender, System::EventArgs^  e) {
		base->EnableDebugPriv();
		if (base->Inject())
		{
			MessageBoxA(NULL, "Dll was Sucessfully injected!", "Sucess!", MB_OK);
		}
		else {
			MessageBoxA(NULL, base->errorMsg.c_str(), "Error!", MB_ICONERROR | MB_OK);
		}
	}
private: System::Void settingBtn_Click(System::Object^  sender, System::EventArgs^  e) {
	OptionForm^ settings = gcnew OptionForm();
	settings->ShowDialog();
	base->options->shouldAutoLoad = settings->autoSaveCBox->Checked;
	base->options->shouldAutoInject = settings->autoInjectCBox->Checked;
	base->options->waitTime = int::Parse(settings->autoInjectWaitTmrTextBox->Text);
}
};
}
