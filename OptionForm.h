#pragma once
//#include "incl.h"

namespace Inject0r {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r OptionForm
	/// </summary>
	public ref class OptionForm : public System::Windows::Forms::Form
	{
	public:
		OptionForm(void)
		{
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~OptionForm()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::CheckBox^  autoSaveCBox;
	protected:
	public: System::Windows::Forms::CheckBox^  autoInjectCBox;


	private: System::Windows::Forms::Label^  autoInjectLbl;
	public: System::Windows::Forms::MaskedTextBox^  autoInjectWaitTmrTextBox;
	private:

	protected:





	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->autoSaveCBox = (gcnew System::Windows::Forms::CheckBox());
			this->autoInjectCBox = (gcnew System::Windows::Forms::CheckBox());
			this->autoInjectLbl = (gcnew System::Windows::Forms::Label());
			this->autoInjectWaitTmrTextBox = (gcnew System::Windows::Forms::MaskedTextBox());
			this->SuspendLayout();
			// 
			// autoSaveCBox
			// 
			this->autoSaveCBox->AutoSize = true;
			this->autoSaveCBox->Location = System::Drawing::Point(12, 12);
			this->autoSaveCBox->Name = L"autoSaveCBox";
			this->autoSaveCBox->Size = System::Drawing::Size(75, 17);
			this->autoSaveCBox->TabIndex = 0;
			this->autoSaveCBox->Text = L"Auto Load";
			this->autoSaveCBox->UseVisualStyleBackColor = true;
			// 
			// autoInjectCBox
			// 
			this->autoInjectCBox->AutoSize = true;
			this->autoInjectCBox->Location = System::Drawing::Point(12, 43);
			this->autoInjectCBox->Name = L"autoInjectCBox";
			this->autoInjectCBox->Size = System::Drawing::Size(77, 17);
			this->autoInjectCBox->TabIndex = 1;
			this->autoInjectCBox->Text = L"Auto Inject";
			this->autoInjectCBox->UseVisualStyleBackColor = true;
			// 
			// autoInjectLbl
			// 
			this->autoInjectLbl->AutoSize = true;
			this->autoInjectLbl->Location = System::Drawing::Point(12, 73);
			this->autoInjectLbl->Name = L"autoInjectLbl";
			this->autoInjectLbl->Size = System::Drawing::Size(72, 13);
			this->autoInjectLbl->TabIndex = 2;
			this->autoInjectLbl->Text = L"Wait For Auto";
			// 
			// autoInjectWaitTmrTextBox
			// 
			this->autoInjectWaitTmrTextBox->Location = System::Drawing::Point(15, 89);
			this->autoInjectWaitTmrTextBox->Mask = L"00000";
			this->autoInjectWaitTmrTextBox->Name = L"autoInjectWaitTmrTextBox";
			this->autoInjectWaitTmrTextBox->Size = System::Drawing::Size(74, 20);
			this->autoInjectWaitTmrTextBox->TabIndex = 4;
			// 
			// OptionForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(110, 117);
			this->Controls->Add(this->autoInjectWaitTmrTextBox);
			this->Controls->Add(this->autoInjectLbl);
			this->Controls->Add(this->autoInjectCBox);
			this->Controls->Add(this->autoSaveCBox);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"OptionForm";
			this->ShowIcon = false;
			this->ShowInTaskbar = false;
			this->Text = L"Settings";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
};
}
